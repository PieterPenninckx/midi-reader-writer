//     Facilitate reading and writing midi files.
//
//     Copyright (C) 2021 Pieter Penninckx
//
//     `midi-reader-writer` is licensed under the Apache License, Version 2.0
//     or the MIT license, at your option.
//
//     For the application of the MIT license, the examples included in the doc comments are not
//     considered "substantial portions of this Software".
//
//     License texts can be found:
//     * for the Apache License, Version 2.0: <LICENSE-APACHE.txt> or
//         <http://www.apache.org/licenses/LICENSE-2.0>
//     * for the MIT license: <LICENSE-MIT.txt> or
//         <http://opensource.org/licenses/MIT>.
//
//

//! Everything specific to using this crate with the [`midly`](https://crates.io/crates/midly) crate,
//! version 0.5.x, behind the `engine-midly-0-5` feature.
//!
//! This module also adds some extra functionality to be used with the `midly` crate:
//! * [`merge_tracks`]: Create an iterator over all tracks, merged (behind the `read` feature)
//! * [`TrackSeparator`]: Separate the tracks again.

/// Re-exports from the [`midly`](https://crates.io/crates/midly) crate, version 0.5.x.
pub mod exports {
    pub use midly_0_5::*;
}

use self::exports::{num::u28, MetaMessage, Track, TrackEvent, TrackEventKind};
#[cfg(all(test, feature = "convert-time"))]
use self::exports::{
    num::{u15, u24},
    Format,
};
#[cfg(all(test, any(feature = "read", feature = "convert-time")))]
use self::exports::{
    num::{u4, u7},
    MidiMessage,
};
#[cfg(feature = "convert-time")]
use self::exports::{Fps, Header, Timing};
#[cfg(feature = "convert-time")]
use crate::{
    ConvertMicroSecondsToTicks, ConvertTicksToMicroseconds, MidiEvent, TimeConversionError,
};
#[cfg(feature = "read")]
use itertools::Itertools; // TODO: remove if not needed.
use std::iter::FromIterator;
#[cfg(feature = "convert-time")]
use std::num::NonZeroU64;
use std::num::TryFromIntError;
use std::{
    convert::TryFrom,
    error::Error,
    fmt::{Display, Formatter},
};
#[cfg(feature = "convert-time")]
use timestamp_stretcher::TimestampStretcher;

#[cfg(feature = "convert-time")]
const MICROSECONDS_PER_SECOND: u64 = 1_000_000;
#[cfg(feature = "convert-time")]
const SECONDS_PER_MINUTE: u64 = 60;
#[cfg(feature = "convert-time")]
const MICROSECONDS_PER_MINUTE: u64 = SECONDS_PER_MINUTE * MICROSECONDS_PER_SECOND;
#[cfg(feature = "convert-time")]
const DEFAULT_BEATS_PER_MINUTE: u64 = 120;

#[cfg(feature = "read")]
/// Create an iterator over all the tracks, merged.
/// The item has type `(u64, usize, TrackEventKind)`,
/// where the first element of the triple is the timing of the event relative to the beginning
/// of the tracks, in ticks,
/// the second item of the triple is the track index and the last item is the event itself.
///
/// # Example
/// ```
/// use midi_reader_writer::midly_0_5::{
///     merge_tracks,
///     exports::{
///         MidiMessage,
///         TrackEvent,
///         TrackEventKind,
///         num::{u15, u4, u7, u28}
///     }
/// };
///
/// // Create a note on event with the given channel
/// fn note_on_with_channel(channel: u8) -> TrackEventKind<'static> {
///     // ...
/// #     TrackEventKind::Midi {
/// #         channel: u4::new(channel),
/// #         message: MidiMessage::NoteOn {
/// #             key: u7::new(1),
/// #             vel: u7::new(1),
/// #         },
/// #     }
/// }
///
/// // Create a note on event with the given delta and channel.
/// fn note_on_with_delta_and_channel(delta: u32, channel: u8) -> TrackEvent<'static> {
///     // ...
/// #     TrackEvent {
/// #        delta: u28::new(delta),
/// #        kind: note_on_with_channel(channel),
/// #    }
/// }
///
/// let tracks = vec![
///     vec![
///         note_on_with_delta_and_channel(2, 0),
///         note_on_with_delta_and_channel(100, 1),
///     ],
///     vec![note_on_with_delta_and_channel(30, 2)],
/// ];
/// let result: Vec<_> = merge_tracks(&tracks[..]).collect();
/// assert_eq!(
///     result,
///     vec![
///         (2, 0, note_on_with_channel(0)),
///         (30, 1, note_on_with_channel(2)),
///         (102, 0, note_on_with_channel(1)),
///     ]
/// )
/// ```
pub fn merge_tracks<'a, 'b>(
    tracks: &'b [Track<'a>],
) -> impl Iterator<Item = (u64, usize, TrackEventKind<'a>)> + 'b {
    let mut track_index = 0;
    tracks
        .iter()
        .map(|t| {
            let mut offset = 0;
            let result = t.iter().map(move |e| {
                offset += e.delta.as_int() as u64;
                (offset, track_index, e.kind)
            });
            track_index += 1;
            result
        })
        .kmerge_by(|(t1, _, _), (t2, _, _)| t1 < t2)
}

#[cfg(feature = "read")]
#[test]
fn merge_tracks_has_sufficiently_flexible_lifetime_annotation() {
    fn wrap(bytes: &[u8]) -> Track {
        let event = TrackEvent {
            delta: u28::new(123),
            kind: TrackEventKind::SysEx(bytes),
        };
        merge_tracks(&[vec![event]])
            .map(|(_, _, kind)| TrackEvent {
                delta: u28::new(0),
                kind,
            })
            .collect()
    }

    let bytes = "abc".to_string(); // Force a non-static lifetime.
    wrap(bytes.as_bytes());
}

#[cfg(feature = "read")]
#[test]
fn merge_tracks_works() {
    fn kind(channel: u8) -> TrackEventKind<'static> {
        TrackEventKind::Midi {
            channel: u4::new(channel),
            message: MidiMessage::NoteOn {
                key: u7::new(1),
                vel: u7::new(1),
            },
        }
    }
    fn track_event(delta: u32, channel: u8) -> TrackEvent<'static> {
        TrackEvent {
            delta: u28::new(delta),
            kind: kind(channel),
        }
    }

    let tracks = vec![
        vec![track_event(1, 0), track_event(2, 1), track_event(4, 2)],
        vec![track_event(2, 3), track_event(2, 4), track_event(5, 5)],
    ];
    let result: Vec<_> = merge_tracks(&tracks[..]).collect();
    assert_eq!(
        result,
        vec![
            (1, 0, kind(0)),
            (2, 1, kind(3)),
            (3, 0, kind(1)),
            (4, 1, kind(4)),
            (7, 0, kind(2)),
            (9, 1, kind(5))
        ]
    )
}

#[cfg(feature = "convert-time")]
impl TryFrom<Header> for ConvertTicksToMicroseconds {
    type Error = TimeConversionError;

    /// Create a new `ConvertTicksToMicrosecond` with the given header.
    ///
    /// The parameter `header` is used to determine the meaning of "tick", since this is stored
    /// in the header in a midi file.
    fn try_from(header: Header) -> Result<Self, Self::Error> {
        let time_stretcher;
        let ticks_per_beat;
        use TimeConversionError::*;
        match header.timing {
            Timing::Metrical(t) => {
                let tpb = NonZeroU64::new(t.as_int() as u64).ok_or(ZeroTicksPerBeatNotSupported)?;
                time_stretcher = TimestampStretcher::new(
                    MICROSECONDS_PER_MINUTE / DEFAULT_BEATS_PER_MINUTE,
                    tpb,
                );
                ticks_per_beat = Some(tpb);
            }
            Timing::Timecode(Fps::Fps29, ticks_per_frame) => {
                // Frames per second = 30 / 1.001 = 30000 / 1001
                // microseconds = ticks * microseconds_per_second / (ticks_per_frame * frames_per_second) ;
                time_stretcher = TimestampStretcher::new(
                    MICROSECONDS_PER_SECOND * 1001,
                    NonZeroU64::new((ticks_per_frame as u64) * 30000)
                        .ok_or(ZeroTicksPerFrameNotSupported)?,
                );
                ticks_per_beat = None;
            }
            Timing::Timecode(fps, ticks_per_frame) => {
                // microseconds = ticks * microseconds_per_second / (ticks_per_frame * frames_per_second) ;
                time_stretcher = TimestampStretcher::new(
                    MICROSECONDS_PER_SECOND,
                    NonZeroU64::new((fps.as_int() as u64) * (ticks_per_frame as u64))
                        .ok_or(ZeroTicksPerFrameNotSupported)?,
                );
                ticks_per_beat = None;
            }
        }
        Ok(Self {
            time_stretcher,
            ticks_per_beat,
        })
    }
}

#[cfg(feature = "convert-time")]
impl<'a> MidiEvent for TrackEventKind<'a> {
    fn tempo(&self) -> Option<u32> {
        if let TrackEventKind::Meta(MetaMessage::Tempo(tempo)) = self {
            Some(tempo.as_int())
        } else {
            None
        }
    }
}

#[cfg(feature = "convert-time")]
#[test]
pub fn convert_ticks_to_microseconds_works_with_one_event() {
    // 120 beats per minute
    // = 120 beats per 60 seconds
    // = 120 beats per 60 000 000 microseconds
    // so the tempo is
    //   60 000 000 / 120 microseconds per beat
    //   = 10 000 000 / 20 microseconds per beat
    //   =    500 000 microseconds per beat
    let tempo_in_microseconds_per_beat = 500000;
    let ticks_per_beat = 32;
    // One event after 1 second.
    // One second corresponds to two beats, so to 64 ticks.
    let event_time_in_ticks: u64 = 64;
    let header = Header {
        timing: Timing::Metrical(u15::from(ticks_per_beat)),
        format: Format::SingleTrack,
    };
    let mut converter =
        ConvertTicksToMicroseconds::try_from(header).expect("No error expected at this point.");
    let microseconds = converter.convert(
        0,
        &TrackEventKind::Meta(MetaMessage::Tempo(u24::from(
            tempo_in_microseconds_per_beat,
        ))),
    );
    assert_eq!(microseconds, 0);
    let microseconds = converter.convert(
        event_time_in_ticks,
        &TrackEventKind::Midi {
            channel: u4::from(0),
            message: MidiMessage::NoteOn {
                key: u7::from(60),
                vel: u7::from(90),
            },
        },
    );
    assert_eq!(microseconds, 1000000);
}

#[cfg(feature = "convert-time")]
impl From<Header> for ConvertMicroSecondsToTicks {
    /// Create a new `ConvertMicroSecondsToTicks` with the given header.
    ///
    /// The parameter `header` is used to determine the meaning of "tick", since this is stored
    /// in the header in a midi file.
    fn from(header: Header) -> Self {
        let time_stretcher;
        let ticks_per_beat;
        match header.timing {
            Timing::Metrical(t) => {
                let tpb = t.as_int() as u64;
                time_stretcher = TimestampStretcher::new(
                    tpb,
                    NonZeroU64::new(MICROSECONDS_PER_MINUTE / DEFAULT_BEATS_PER_MINUTE)
                        .expect("Bug: MICROSECONDS_PER_MINUTE / DEFAULT_BEATS_PER_MINUTE should not be zero."),
                );
                ticks_per_beat = Some(tpb);
            }
            Timing::Timecode(Fps::Fps29, ticks_per_frame) => {
                // Frames per second = 30 / 1.001 = 30000 / 1001
                // ticks = microseconds * ticks_per_frame * frames_per_second / microseconds_per_second
                time_stretcher = TimestampStretcher::new(
                    (ticks_per_frame as u64) * 30000,
                    NonZeroU64::new(MICROSECONDS_PER_SECOND * 1001)
                        .expect("Bug: MICROSECONDS_PER_SECOND * 1001 should not be zero."),
                );
                ticks_per_beat = None;
            }
            Timing::Timecode(fps, ticks_per_frame) => {
                // ticks = microseconds * ticks_per_frame * frames_per_second / microseconds_per_second
                time_stretcher = TimestampStretcher::new(
                    (fps.as_int() as u64) * (ticks_per_frame as u64),
                    NonZeroU64::new(MICROSECONDS_PER_SECOND)
                        .expect("Bug: MICROSECONDS_PER_SECOND should not be zero."),
                );
                ticks_per_beat = None;
            }
        }
        Self {
            time_stretcher,
            ticks_per_beat,
        }
    }
}

#[cfg(feature = "convert-time")]
#[test]
pub fn convert_microseconds_to_ticks_works_with_one_event() {
    // 120 beats per minute
    // = 120 beats per 60 seconds
    // = 120 beats per 60 000 000 microseconds
    // so the tempo is
    //   60 000 000 / 120 microseconds per beat
    //   = 10 000 000 / 20 microseconds per beat
    //   =    500 000 microseconds per beat
    let tempo_in_microseconds_per_beat = 500000;
    let ticks_per_beat = 32;
    // One event after 1 second.
    // One second corresponds to two beats, so to 64 ticks.
    let expected_vent_time_in_ticks: u64 = 64;
    let event_time_in_microseconds: u64 = 1000000;
    let header = Header {
        timing: Timing::Metrical(u15::from(ticks_per_beat)),
        format: Format::SingleTrack,
    };
    let mut converter = ConvertMicroSecondsToTicks::from(header);
    let microseconds = converter
        .convert(
            0,
            &TrackEventKind::Meta(MetaMessage::Tempo(u24::from(
                tempo_in_microseconds_per_beat,
            ))),
        )
        .expect("No error expected at this point.");
    assert_eq!(microseconds, 0);
    let observed_event_time_in_ticks = converter
        .convert(
            event_time_in_microseconds,
            &TrackEventKind::Midi {
                channel: u4::from(0),
                message: MidiMessage::NoteOn {
                    key: u7::from(60),
                    vel: u7::from(90),
                },
            },
        )
        .expect("No error expected at this point");
    assert_eq!(expected_vent_time_in_ticks, observed_event_time_in_ticks);
}

/// The error type returned by the [`TrackSeparator::push()`] method.
#[derive(Debug)]
#[non_exhaustive]
pub enum SeparateTracksError {
    /// The specified time overflows what can be specified.
    TimeOverflow,
    /// Time decreased from one event to a next event.
    TimeCanOnlyIncrease,
}

impl From<TryFromIntError> for SeparateTracksError {
    fn from(_: TryFromIntError) -> Self {
        SeparateTracksError::TimeOverflow
    }
}

impl Display for SeparateTracksError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            SeparateTracksError::TimeOverflow => {
                write!(
                    f,
                    "the time overflows what can be represented in a midi file."
                )
            }
            SeparateTracksError::TimeCanOnlyIncrease => {
                write!(f, "subsequent events with decreasing time found.")
            }
        }
    }
}

impl Error for SeparateTracksError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

/// Write to a track, keeping ... well, keeping "track" of the timing relative to the beginning.
#[derive(Clone)]
struct TrackWriter<'a> {
    track: Track<'a>,
    ticks: u64,
}

impl<'a> TrackWriter<'a> {
    fn new() -> Self {
        TrackWriter {
            track: Vec::new(),
            ticks: 0,
        }
    }

    fn push(&mut self, ticks: u64, kind: TrackEventKind<'a>) -> Result<(), SeparateTracksError> {
        use SeparateTracksError::*;
        if self.ticks > ticks {
            return Err(TimeCanOnlyIncrease);
        }
        let delta = ticks - self.ticks;
        let delta = u28::try_from(u32::try_from(delta)?).ok_or(TimeOverflow)?;
        self.ticks = ticks;
        self.track.push(TrackEvent { delta, kind });
        Ok(())
    }
}

/// Separate events into different tracks.
pub struct TrackSeparator<'a> {
    tracks: Vec<TrackWriter<'a>>,
}

impl<'a> TrackSeparator<'a> {
    /// Create a new `TrackSeparator`.
    ///
    /// # Example
    /// ```
    /// use midi_reader_writer::midly_0_5::TrackSeparator;
    /// let track_separator = TrackSeparator::new();
    /// let tracks : Vec<_> = track_separator.collect();
    /// assert!(tracks.is_empty());
    /// ```
    #[inline]
    pub fn new() -> Self {
        TrackSeparator { tracks: Vec::new() }
    }

    /// Push a new event.
    ///
    /// # Parameters
    /// * `ticks`: the time in midi ticks, relative to the beginning
    ///   of the tracks
    /// * `track_index`: the index of the track to which the event belongs
    /// * `event`: the event
    ///
    /// # Example
    /// ```
    /// use midi_reader_writer::midly_0_5::{TrackSeparator, exports::{TrackEventKind, TrackEvent, MetaMessage}};
    /// # use midi_reader_writer::midly_0_5::exports::{
    /// #     MidiMessage,
    /// #     num::{u4, u7, u28}
    /// # };
    ///
    /// // Create a note on event with the given channel
    /// fn note_on_with_channel(channel: u8) -> TrackEventKind<'static> {
    ///     // ...
    /// #     TrackEventKind::Midi {
    /// #         channel: u4::new(channel),
    /// #         message: MidiMessage::NoteOn {
    /// #             key: u7::new(1),
    /// #             vel: u7::new(1),
    /// #         },
    /// #     }
    /// }
    ///
    /// // Create a note on event with the given delta and channel.
    /// fn note_on_with_delta_and_channel(delta: u32, channel: u8) -> TrackEvent<'static> {
    ///     // ...
    /// #     TrackEvent {
    /// #        delta: u28::new(delta),
    /// #        kind: note_on_with_channel(channel),
    /// #    }
    /// }
    ///
    /// // Create an end-of-track event with the given delta.
    /// fn end_of_track(delta: u32) -> TrackEvent<'static> {
    ///     // ...
    /// #     TrackEvent {
    /// #       delta: u28::new(delta),
    /// #       kind: TrackEventKind::Meta(MetaMessage::EndOfTrack)
    /// #    }
    /// }
    ///
    /// let mut track_separator = TrackSeparator::new();
    /// track_separator.push(5, 0, note_on_with_channel(0));
    /// track_separator.push(0, 1, note_on_with_channel(1));
    /// track_separator.push(10, 0, note_on_with_channel(2));
    /// track_separator.push(3, 1, TrackEventKind::Meta(MetaMessage::EndOfTrack));
    /// let tracks : Vec<_> = track_separator.collect();
    /// assert_eq!(tracks.len(), 2);
    /// assert_eq!(
    ///             tracks[0],
    ///             vec![
    ///                 note_on_with_delta_and_channel(5, 0),
    ///                 note_on_with_delta_and_channel(5, 2),
    ///                 end_of_track(0)
    ///             ]
    /// );
    /// assert_eq!(tracks[1], vec![note_on_with_delta_and_channel(0, 1), end_of_track(3)]);
    /// ```

    #[inline]
    pub fn push(
        &mut self,
        ticks: u64,
        track_index: usize,
        event: TrackEventKind<'a>,
    ) -> Result<(), SeparateTracksError> {
        if self.tracks.len() <= track_index {
            self.tracks.resize(track_index + 1, TrackWriter::new());
        }
        self.tracks[track_index].push(ticks, event)
    }

    /// Append all events from an iterator of triples of type `(u64, usize, TrackEventKind)`,
    /// where
    /// * the first item of the triple (of type `u64`) is the time in midi ticks, relative to the beginning
    ///   of the tracks
    /// * the second item of the triple (of type `usize`) is the track index.
    /// * the last item of the triple is the event itself.
    /// # Example
    /// ```
    /// use midi_reader_writer::midly_0_5::{TrackSeparator, exports::{TrackEventKind, TrackEvent}};
    /// # use midi_reader_writer::midly_0_5::exports::{MetaMessage, MidiMessage, num::{u4, u7, u28}};
    ///
    /// fn note_on_with_channel(channel: u8) -> TrackEventKind<'static> {
    ///     // ...
    /// #    TrackEventKind::Midi {
    /// #        channel: u4::new(channel),
    /// #        message: MidiMessage::NoteOn {
    /// #            key: u7::new(1),
    /// #            vel: u7::new(1),
    /// #        },
    /// #    }
    /// }
    ///
    /// fn note_on_with_channel_and_delta_time(delta: u32, channel: u8) -> TrackEvent<'static> {
    ///     // ...
    /// #    TrackEvent {
    /// #        delta: u28::new(delta),
    /// #        kind: note_on_with_channel(channel),
    /// #    }
    /// }
    /// // Create an end-of-track event with the given delta.
    /// fn end_of_track(delta: u32) -> TrackEvent<'static> {
    ///     // ...
    /// #     TrackEvent {
    /// #       delta: u28::new(delta),
    /// #       kind: TrackEventKind::Meta(MetaMessage::EndOfTrack)
    /// #    }
    /// }
    ///
    /// let events : Vec<(u64, usize, _)>= vec![
    ///     (1, 0, note_on_with_channel(0)),
    ///     (2, 1, note_on_with_channel(3)),
    ///     (3, 0, note_on_with_channel(1)),
    ///     (4, 1, note_on_with_channel(4)),
    ///     (7, 0, note_on_with_channel(2)),
    ///     (9, 1, note_on_with_channel(5)),
    /// ];
    ///
    /// let mut separator = TrackSeparator::new();
    /// separator.extend(events).expect("No error should occur here.");
    /// let separated : Vec<_> = separator.collect();
    ///
    /// assert_eq!(
    ///     separated,
    ///     vec![
    ///         vec![
    ///             note_on_with_channel_and_delta_time(1, 0),
    ///             note_on_with_channel_and_delta_time(2, 1),
    ///             note_on_with_channel_and_delta_time(4, 2),
    ///             end_of_track(0)
    ///         ],
    ///         vec![
    ///             note_on_with_channel_and_delta_time(2, 3),
    ///             note_on_with_channel_and_delta_time(2, 4),
    ///             note_on_with_channel_and_delta_time(5, 5),
    ///             end_of_track(0)
    ///         ],
    ///     ]
    /// );
    /// ```
    #[inline]
    pub fn extend<I>(&mut self, iter: I) -> Result<(), SeparateTracksError>
    where
        I: IntoIterator<Item = (u64, usize, TrackEventKind<'a>)>,
    {
        for (ticks, track_index, event) in iter {
            self.push(ticks, track_index, event)?
        }
        Ok(())
    }

    /// Create a collection containing all the tracks.
    ///
    /// The number of tracks is determined by the highest track index.
    ///
    /// An [`EndOfTrack`](exports::MetaMessage::EndOfTrack) event is added to the tracks that do not end with an `EndOfTrack` event.
    pub fn collect<B>(self) -> B
    where
        B: FromIterator<Track<'a>>,
    {
        self.tracks
            .into_iter()
            .map(|t| {
                let mut track = t.track;
                let end_of_track_is_marked_with_event = if let Some(last_event) = track.last() {
                    last_event.kind == TrackEventKind::Meta(MetaMessage::EndOfTrack)
                } else {
                    false
                };
                if !end_of_track_is_marked_with_event {
                    track.push(TrackEvent {
                        kind: TrackEventKind::Meta(MetaMessage::EndOfTrack),
                        delta: u28::new(0),
                    });
                }
                track
            })
            .collect()
    }
}
