Version 0.1.2
=============
* Automatically add an `EndOfTrack` event in the `collect` method of `TrackSeparator`.

Version 0.1.1
=============
* Remove incorrect trait bound from the `merge_tracks` function.

Version 0.1.0
=============
Initial release
